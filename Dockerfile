# Copyright (c) 2018, ARM Limited.
# SPDX-License-Identifier: Apache-2.0

ARG ARM_ARCH=arm64v8
FROM ${ARM_ARCH}/ubuntu:bionic

COPY ./qemu-user-static/* /usr/bin/
COPY ./ros2_ws/src /ros2_ws/src

# Set timezone
RUN echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get update && apt-get install -q -y tzdata && rm -rf /var/lib/apt/lists/*

RUN apt update && apt install -y \
        software-properties-common \
        lsb-release \
        curl \
        gnupg2 \
        build-essential \
        cmake \
        git \
        wget

ENV LANG en_US.UTF-8
ENV LC_ALL C.UTF-8

RUN curl http://repo.ros2.org/repos.key | apt-key add -
RUN sh -c 'echo "deb [arch=amd64,arm64] http://repo.ros2.org/ubuntu/main \
    `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'

RUN apt update && apt install -y \
  python3-colcon-common-extensions \
  python3-flake8 \
  python3-pip \
  python3-pytest-cov \
  python3-rosdep \
  python3-setuptools

RUN python3 -m pip install -U \
        flake8-blind-except \
        flake8-builtins \
        flake8-class-newline \
        flake8-comprehensions \
        flake8-deprecated \
        flake8-docstrings \
        flake8-import-order \
        flake8-quotes \
        pytest-repeat \
        pytest-rerunfailures \
        pytest \
        setuptools
RUN python3 -m pip install -U importlib-metadata importlib-resources

WORKDIR /ros2_ws
RUN rosdep init
RUN rosdep update --rosdistro galactic
RUN rosdep install --from-paths src \
    --ignore-src \
    --rosdistro galactic -y \
    --skip-keys "\
        fastcdr \
        rti-connext-dds-5.3.1 \
        urdfdom_headers"

RUN mkdir -p install build/cyclonedds && \
    cd build/cyclonedds && \
    cmake -DCMAKE_INSTALL_PREFIX=../../install ../../src/eclipse-cyclonedds/cyclonedds/ -DBUILD_IDLC=OFF && \
    cmake --build . && \
    cmake --build . --target install

WORKDIR /
RUN apt-get install -y symlinks
RUN symlinks -rc .
