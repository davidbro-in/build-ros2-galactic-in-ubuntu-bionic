terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
# Configure the AWS Provider
provider "aws" {
  region = "us-west-1"
}

variable "environment_tag" {
  description = "Environment tag"
  default     = "Production"
}

variable "public_key" {
  type = string
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "main" {
  cidr_block = "10.0.0.0/24"
  vpc_id     = aws_vpc.main.id
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id
  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      gateway_id                 = aws_internet_gateway.main.id
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      nat_gateway_id             = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    }
  ]
}

resource "aws_route_table_association" "main" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.main.id
}

resource "aws_security_group" "sg_22" {
  name        = "sg_22"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress = [
    {
      description      = "SSH from everywhere"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      prefix_list_ids  = []
      security_groups  = []
      self             = false
      ipv6_cidr_blocks = []
    }
  ]

  egress = [
    {
      description      = "All ports output"
      prefix_list_ids  = []
      security_groups  = []
      self             = false
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_network_interface" "main" {
  subnet_id       = aws_subnet.main.id
  private_ips     = ["10.0.0.12"]
  security_groups = [aws_security_group.sg_22.id]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.main.id
  associate_with_private_ip = "10.0.0.12"
  depends_on                = [aws_internet_gateway.main]
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = format("ssh-ed25519 %s davidmbroin@gmail.com", var.public_key)
}

resource "aws_spot_instance_request" "cheap_worker" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "m5a.8xlarge"

  key_name = aws_key_pair.deployer.key_name

  root_block_device {
    volume_size = 100
  }

  tags = {
    Environment = "${var.environment_tag}"
  }

  depends_on = [aws_internet_gateway.main]

  network_interface {
    network_interface_id = aws_network_interface.main.id
    device_index         = 0
  }
}
