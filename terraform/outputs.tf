### The Ansible inventory file
resource "local_file" "AnsibleInventory" {
  content = templatefile("../ansible/inventory.tmpl",
    {
      bastion-dns = aws_eip.one.public_dns,
      bastion-ip  = aws_eip.one.public_ip,
      bastion-id  = aws_eip.one.id
    }
  )
  filename = "../ansible/inventory"
}

### Dynamic URL from AWS instance
resource "local_file" "deployURL" {
  content = templatefile("deploy.env.tmpl",
    {
      bastion-ip = aws_eip.one.public_ip
    }
  )
  filename = "../deploy.env"
}
